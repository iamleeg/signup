#ifndef GNUSTEP
#include <GNUstepBase/GNUstep.h>
#endif

#import "Main.h"

@interface Main ()

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *header;

@end

@implementation Main

- (instancetype) init
{
  self = [super init];
  if (self)
    {
      _title = @"APPropriate Behaviour: print edition";
      _header = @"Sign up for the APPropriate Behaviour print edition!";
    }
  return self;
}

@end
