// _Person.m
//
// Created by EOModelEditor.
// DO NOT EDIT. Make changes to Person.m instead.

#import "_Person.h"

@implementation _Person

- (void) dealloc
{
  [_email release];
  [_personName release];
  [_wouldPay release];

  [super dealloc];
}

- (void) setEmail:(NSString *) aValue
{
  if (_email == aValue) {
    return;
  }

  [self willChange];
  ASSIGN(_email, aValue);
}

- (NSString *) email
{
  return _email;
}

- (void) setPersonName:(NSString *) aValue
{
  if (_personName == aValue) {
    return;
  }

  [self willChange];
  ASSIGN(_personName, aValue);
}

- (NSString *) personName
{
  return _personName;
}

- (void) setWouldPay:(NSDecimalNumber *) aValue
{
  if (_wouldPay == aValue) {
    return;
  }

  [self willChange];
  ASSIGN(_wouldPay, aValue);
}

- (NSDecimalNumber *) wouldPay
{
  return _wouldPay;
}

@end
