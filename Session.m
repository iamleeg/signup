#import "Session.h"

@implementation Session

- (instancetype)init
{
  self = [super init];
  if (self)
    {
      [self setStoresIDsInCookies: YES];
      [self setStoresIDsInURLs: NO];
    }
  return self;
}
@end
