#import "AddPersonCommandHandler.h"
#import "AddPersonCommand.h"
#import "PersonSupply.h"
#import "Person.h"

@implementation AddPersonCommandHandler

- (BOOL)canHandleCommand: (id <IKBCommand>)command
{
  return [command isKindOfClass: [AddPersonCommand class]];
}

- (void)executeCommand: (AddPersonCommand *)command
{
  NSParameterAssert([command isKindOfClass: [AddPersonCommand class]]);
  PersonSupply *supplier = [PersonSupply new];
  Person *newPerson = nil;
  if (command.shouldContact)
    newPerson = [supplier personWithEmail: command.email];
  else
    newPerson = [supplier insertedPerson];
  [newPerson setPersonName: command.name];
  if (command.willingToPay)
    [newPerson setWouldPay: command.willingToPay];
  [supplier saveChanges];
  [supplier release];
}

@end
